package fr.univ_smb.isc.m2.integration_tests.customer;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URISyntaxException;
import java.net.URL;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.HttpClientBuilder;
import org.junit.Test;

import static org.apache.http.HttpStatus.SC_NOT_FOUND;
import static org.apache.http.HttpStatus.SC_OK;


public class CharacterGetIt {
	
    @Test
    public void should_200_On_Existing_Character() throws IOException, URISyntaxException {


        HttpUriRequest request = new HttpGet(new URL("http://localhost:" + 9135 + "/api/characters/" + 1).toURI());

        HttpResponse response = HttpClientBuilder.create().build().execute(request);

        assertThat(response.getStatusLine().getStatusCode()).isEqualTo(SC_OK);

    }

    @Test
    public void should_404_On_Non_Existing_Character() throws IOException, URISyntaxException {

        HttpUriRequest request = new HttpGet(new URL("http://localhost:" + 9135 + "/api/Characters/" + 1024).toURI());

        HttpResponse response = HttpClientBuilder.create().build().execute(request);

        assertThat(response.getStatusLine().getStatusCode()).isEqualTo(SC_NOT_FOUND);

    }

    @Test
    public void should_200_On_all_Characters() throws IOException, URISyntaxException {

        HttpUriRequest request = new HttpGet(new URL("http://localhost:" + 9135 + "/api/characters/").toURI());

        HttpResponse response = HttpClientBuilder.create().build().execute(request);

        assertThat(response.getStatusLine().getStatusCode()).isEqualTo(SC_OK);

    }



    @Test
    public void should_create_on_addcharacter() throws IOException, URISyntaxException {


        HttpUriRequest request = new HttpGet(new URL("http://localhost:" + 9135 +
                "/api/addCharacter?name=Test&classe=Test&race=Test&guilde=Test&niveau=1").toURI());

        HttpResponse response = HttpClientBuilder.create().build().execute(request);

        assertThat(response.getStatusLine().getStatusCode()).isEqualTo(SC_OK);

    }

    @Test
    public void character_should_have_been_created() throws IOException, URISyntaxException {

        HttpUriRequest request = new HttpGet(new URL("http://localhost:" + 9135 + "/api/characters/" + 2).toURI());

        HttpResponse response = HttpClientBuilder.create().build().execute(request);
        HttpEntity entity = response.getEntity();

        InputStream inputStream = entity.getContent();

        BufferedReader streamReader = new BufferedReader(new InputStreamReader( inputStream, "UTF-8"));
        StringBuilder responseStrBuilder = new StringBuilder();

        String inputStr;
        while ((inputStr = streamReader.readLine()) != null) responseStrBuilder.append(inputStr);

        assertThat(responseStrBuilder.toString().equals("[\n" +
                "{\n" +
                "id: 2,\n" +
                "name: Test\n" +
                "classe: Test\n" +
                "race: Test\n" +
                "guilde: Test\n" +
                "niveau: Test\n" +
                "}\n" +
                "]")).isEqualTo(true);
    }



    
    @Test
    public void should_404_on_non_existing_character_delete() throws IOException, URISyntaxException {


        HttpUriRequest request = new HttpGet(new URL("http://localhost:" + 9135 + "/deleteCharacter?id=1024").toURI());

        HttpResponse response = HttpClientBuilder.create().build().execute(request);

        assertThat(response.getStatusLine().getStatusCode()).isEqualTo(SC_NOT_FOUND);

    }

    @Test
    public void should_delete_on_delete() throws IOException, URISyntaxException {


        HttpUriRequest request = new HttpGet(new URL("http://localhost:" + 9135 + "/deleteCharacter?id=2").toURI());

        HttpResponse response = HttpClientBuilder.create().build().execute(request);

        assertThat(response.getStatusLine().getStatusCode()).isEqualTo(SC_OK);

    }

    @Test
    public void weapon_should_have_been_deleted() throws IOException, URISyntaxException {

        HttpUriRequest request = new HttpGet(new URL("http://localhost:" + 9135 + "/api/characters/" + 2).toURI());

        HttpResponse response = HttpClientBuilder.create().build().execute(request);

        assertThat(response.getStatusLine().getStatusCode()).isEqualTo(SC_NOT_FOUND);

    }

}
