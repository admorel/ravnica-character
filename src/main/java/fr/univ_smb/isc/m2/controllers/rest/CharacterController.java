package fr.univ_smb.isc.m2.controllers.rest;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import fr.univ_smb.isc.m2.config.rest.ResourceNotFoundException;
import fr.univ_smb.isc.m2.domain.character.Character;
import fr.univ_smb.isc.m2.domain.character.CharacterRepository;


@RestController
@RequestMapping("/api")
public class CharacterController {

	private final CharacterRepository characterRepository;

    @Autowired()
    public CharacterController(CharacterRepository characterRepository) {
        
        this.characterRepository = characterRepository;
    }


    @RequestMapping(value = "/characters", method = RequestMethod.GET)
    public ModelAndView character() {
    	ModelAndView mv = new ModelAndView("allcharacter");
    	
    	mv.addObject("listCharacter", characterRepository.findAll());
    	return mv;
    }

    @RequestMapping(value = "/characters/{id}", method = RequestMethod.GET)
    public ModelAndView character(@PathVariable String id) {

    	ModelAndView mv = new ModelAndView("character");
    	Character character = characterRepository.findOne(Integer.parseInt(id));

        if (character == null) {
            throw new ResourceNotFoundException();
        }
        
        mv.addObject("character", character);

        return mv;
    }
    
    @RequestMapping(value = "/characters/{guild}", method = RequestMethod.GET)
    public ModelAndView charactersByGuild(@PathVariable String guild) {

    	ModelAndView mv = new ModelAndView("characterByGuild");
    	List<Character> allCharacters = characterRepository.findAll();
    	List<Character> guildCharacters = new ArrayList<Character>();
    	
    	for(Character c: allCharacters) {
    		if(c.getGuilde().equals(guild)) allCharacters.add(c);
    	}

        
        mv.addObject("guildCharacters", guildCharacters);

        return mv;
    }

    @RequestMapping(value = "/deleteCharacter", method = RequestMethod.GET)
    public ModelAndView deleteCharacter(HttpServletRequest request, @RequestParam String id) {
        if(request.isUserInRole("ADMIN")) {
        	characterRepository.delete(Integer.parseInt(id));
        }
        
        return character();
    }

    @RequestMapping(value = "/addCharacter", method = RequestMethod.GET)
    public ModelAndView createCharacter(HttpServletRequest request,
    		@RequestParam String name, @RequestParam String classe, @RequestParam String race,
			@RequestParam String guilde, @RequestParam String niveau) {

        if(request.isUserInRole("ADMIN")){
            Character character = new Character(name, classe, race, guilde, Integer.parseInt(niveau));
            characterRepository.saveAndFlush(character);
            
        }
        
        return character();

    }
    
    
    @RequestMapping(value = "/updateCharacter", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
	public ModelAndView updateCharacter(HttpServletRequest request, @RequestParam String id, @RequestParam String name, @RequestParam String classe, @RequestParam String race,
			@RequestParam String guilde, @RequestParam String niveau ) {
		
    	Character character = characterRepository.findOne(Integer.parseInt(id));

        if (character == null) {
            throw new ResourceNotFoundException();
        }
		
		if(request.isUserInRole("ADMIN")){
			if(name != "") {
				character.setName(name);
			}
			if(classe != "") {
				character.setClasse(classe);
			}
			if(race != "") {
				character.setRace(race);
			}
			if(guilde != "") {
				character.setGuilde(guilde);
			}
			if(niveau != "") {
				character.setNiveau(Integer.parseInt(niveau));
			}
		
			characterRepository.saveAndFlush(character);
			
		}
		return character();
	}
    
    
    
    


}
