package fr.univ_smb.isc.m2.domain.character;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Character implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 4965018405164563181L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	int id;

	private String name;

	private String classe;

	private String race;

	private String guilde;

	private int niveau;
	
	public Character() {
		super();
	}

	public Character(String name, String classe, String race, String guilde, int niveau) {
		super();
		this.name = name;
		this.classe = classe;
		this.race = race;
		this.guilde = guilde;
		this.niveau = niveau;
	}

	public int getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getClasse() {
		return classe;
	}

	public void setClasse(String classe) {
		this.classe = classe;
	}

	public String getRace() {
		return race;
	}

	public void setRace(String race) {
		this.race = race;
	}

	public String getGuilde() {
		return guilde;
	}

	public void setGuilde(String guilde) {
		this.guilde = guilde;
	}

	public int getNiveau() {
		return niveau;
	}

	public void setNiveau(int niveau) {
		this.niveau = niveau;
	}

}
