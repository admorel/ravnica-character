package fr.univ_smb.isc.m2.domain.character;

import static java.util.stream.Collectors.toList;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

@Service
public class CharacterService {

	private ArrayList<Character> characters;

	public CharacterService() {
		characters = new ArrayList<>();
		



	}

	public List<Character> all() {
		return characters;
	}

	public Character selectById(int characterId) {
		List<Character> collect = characters.stream().filter(u -> u.id == characterId).collect(toList());

		if (collect.isEmpty()) {
			return null;
		} else {
			return collect.get(0);
		}
	}
	
	public Character selectByName(String characterName) {
		List<Character> collect = characters.stream().filter(u -> u.getName() == characterName).collect(toList());

		if (collect.isEmpty()) {
			return null;
		} else {
			return collect.get(0);
		}
	}

	public Character create(String characterName, String characterClasse, String characterRace, String characterGuilde,
			int characterNiveau) {
		Character newCharacter = new Character(characterName, characterClasse, characterRace, characterGuilde,
				characterNiveau);
		characters.add(newCharacter);
		return newCharacter;
	}

	public void delete(int id) {
        for (Character c : characters) {
            if(c.getId() == id)
            	characters.remove(c);
        }
	}

}
