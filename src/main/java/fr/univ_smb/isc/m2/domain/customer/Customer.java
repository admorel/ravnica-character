package fr.univ_smb.isc.m2.domain.customer;

public class Customer {

    public String firstName;
    public String lastName;
    public int id;

    public Customer() {
        // Jackson
    }

    public Customer(int id, String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public int getId() {
        return id;
    }

}
