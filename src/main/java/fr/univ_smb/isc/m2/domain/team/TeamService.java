package fr.univ_smb.isc.m2.domain.team;

import static java.util.stream.Collectors.toList;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import fr.univ_smb.isc.m2.domain.character.Character;
import fr.univ_smb.isc.m2.domain.character.CharacterService;

@Service
public class TeamService {

	private ArrayList<Team> teams;

	public TeamService() {
		teams = new ArrayList<>();
	}
	

	public List<Team> all() {
		return teams;
	}

	public Team selectById(int teamId) {
		List<Team> collect = teams.stream().filter(u -> u.id == teamId).collect(toList());

		if (collect.isEmpty()) {
			return null;
		} else {
			return collect.get(0);
		}
	}

	public Team create(String teamName, int teamNiveau, String... characterName) {
		
		List<Character> memberTeam = new ArrayList<Character>();
		
		for(String s: characterName) {
			CharacterService newCarAS = new CharacterService();
			
			memberTeam.add(newCarAS.selectByName(s));
			
		}
		
		Team newTeam = new Team(teamName, memberTeam, teamNiveau);
		teams.add(newTeam);
		return newTeam;
	}

	public void deleteTeamById(int idTeam) {
        for (Team c : teams) {
            if(c.getId() == idTeam)
            	teams.remove(c);
        }
	}
	
	public void deleteTeamByName(int idTeam) {
        for (Team c : teams) {
            if(c.getId() == idTeam)
            	teams.remove(c);
        }
	}
	
	public void deleteMemberTeam(int id) {
        for (Team c : teams) {
            if(c.getId() == id)
            	teams.remove(c);
        }
	}


}
