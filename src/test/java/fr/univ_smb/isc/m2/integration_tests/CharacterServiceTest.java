package fr.univ_smb.isc.m2.integration_tests;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import fr.univ_smb.isc.m2.domain.character.Character;
import fr.univ_smb.isc.m2.domain.character.CharacterRepository;
import fr.univ_smb.isc.m2.domain.character.CharacterService;

public class CharacterServiceTest {

	
	@Test
	public void shouldSelectTheGoodAcolyte() {
		CharacterService as = new CharacterService();

		Character a = as.create("Eole", "Barde", "Humain", "Rakdos", 1);

		assertTrue(as.selectById(0) == a);
	}

	@Test
	public void shouldAddAcolyte() {
		CharacterService as = new CharacterService();
		Character a = as.create("Eole", "Barde", "Humain", "Rakdos", 1);

		assertTrue(as.all().get(0) == a);
	}

	@Test
	public void shouldAddAcolyteToEndofList() {
		CharacterService as = new CharacterService();
		Character a = as.create("Eole", "Barde", "Humain", "Rakdos", 1);
		Character b = as.create("Nikya", "Druide", "Centaure", "Gruul", 18);

		assertTrue(as.all().get(as.all().size() - 1) == b);
	}

	@Test
	public void shouldSelectTheBadAcolyte() {
		CharacterService as = new CharacterService();

		Character a = as.create("Eole", "Barde", "Humain", "Rakdos", 1);
		Character b = as.create("Nikya", "Druide", "Centaure", "Gruul", 18);

		assertFalse(as.selectById(0) == b);
	}
	

}
